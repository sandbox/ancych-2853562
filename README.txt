GENERAL
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
This module redirects a user to a specific URL after login or logout.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/user_login_logout_redirect

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/user_login_logout_redirect

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  * Place the Redirect After Logout module into your modules directory.
    This is normally the "sites/all/modules" directory.

  * Go to admin/build/modules. Enable the module.

  * The Redirect After Logout module is found in the "Other" section.

CONFIGURATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  1. Go to: admin/config/people/user_login_logout_redirect

    -> Set the login destination url. Leave empty to disable.

    -> Set the logout destination url. Leave empty to disable.

  2. Got to: admin/people/permissions/roles

    -> Search for: User login & logout redirect

    -> Set permissions by role. Only roles with the permission to use the
      user login-logout redirect module will be redirected.


MAINTAINERS
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Current maintainers:
 * Equipo ANCYCH (ancych): https://www.ancych.com
